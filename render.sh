#!/usr/bin/env bash
fn=90;
HERE="$(dirname "$(readlink -f "$0")")"
j=8
OUT="$HERE/out"
echo "$HERE/parts.scad"
mkdir -p "$OUT"

function render() {
    grep < parts.scad "// $1$" \
        | grep -oP '(?<=part==").+?(?=")' \
        | parallel --jobs "$j" -- \
            openscad --hardwarnings \
                --enable textmetrics \
                --enable roof \
                --enable manifold \
                -D 'part=\"{}\"' \
                -o "$OUT/{}.$2" \
                "$HERE/parts.scad"
}

render 3d stl
render 2d dxf
