include <ref/derived.scad>;
include <util/round.scad>;
use <ducts.scad>;
use <carriage.scad>;
use <hotend.scad>;
use <body_cuts.scad>;

orbiter_fan_shroud_od = orbiter_motor_dia + orbiter_motor_fan_clearance*2 + base_baffle_wall_w;
module body_yz() {
    translate([back_y_pos, bottom_z_pos]) square([front_y_pos - back_y_pos, orbiter_plate_h-bottom_z_pos]);
    round_inner(base_wall_w) translate([orbiter_fan_pos[1], orbiter_fan_pos[2]]) {
        circle(d=orbiter_fan_shroud_od);
        round_outer(fan_r) translate([0, -fan_r]) square([fan_side, fan_side + fan_r*2], center=true);
        at_orbiter_screws() circle(d=orbiter_motor_screw_standoff_dia + base_baffle_wall_w*2);
    }
}

module body_bottom_xz() translate([left_x_pos, bottom_z_pos]) square([x_size, -bottom_z_pos + orbiter_plate_h]);
module body_xz() {
    body_bottom_xz();
    translate([orbiter_body_motor_side_l, 0]) square([orbiter_fan_pos[0] - orbiter_body_motor_side_l, orbiter_plate_h + orbiter_motor_z + orbiter_motor_dia/2 + orbiter_motor_fan_clearance + base_baffle_wall_w]);
}

module body() difference() {
    union() {
        intersection() {
            rotate([90, 0, 90]) linear_extrude(999, center=true) body_yz();
            rotate([90, 0, 0]) linear_extrude(999, center=true) body_xz();
        }
        at_front_pcb() linear_extrude(screw_insert_h) intersection() {
            at_front_pcb_holes() offset(r=screw_insert_dia/2 + base_baffle_wall_w) square(999);
            difference() {
                body_bottom_xz();
                translate([orbiter_plate_size[0]/2 + base_assembly_wiggle, 0]) mirror([1, 0]) square(999);
            }
            translate([0, -base_baffle_wall_w]) body_bottom_xz();
        }
    }
    // clearance for carriage end
    body_cut_carriage_end_clearance();
    // orbiter plate cutout
    linear_extrude(999) translate([orbiter_plate_size[0]/2, -orbiter_plate_size[1]/2 + orbiter_plate_offset]) offset(delta=base_assembly_wiggle) round_outer(orbiter_plate_r) mirror([1, 0]) {
        square([orbiter_plate_size[0], 999]);
    }
    // hotend fan duct
    hotend_fan_duct();
    // orbiter motor wire passthrough
    orbiter_motor_wire_passthrough();
    // orbiter motor wire installation cutout
    orbiter_motor_installation_cutout();
    // hotend heatsink cut
    cylinder(d=hotend_heatsink_dia, h=999, center=true);
    // hotend heaterblock cut
    translate(hotend_heaterblock_pos) translate([-999, 0, 0]) cube([hotend_heaterblock_size[0] + 999, hotend_heaterblock_size[1], hotend_heaterblock_size[2]]);
    // hotend wiring duct
    hotend_wiring_duct();
    // orbiter fan screw holes
    translate(orbiter_fan_pos) rotate([0, 90, 0]) at_fan_screws(3) cylinder(d=screw_insert_dia, h=screw_insert_h*2, center=true);
    // heaterblock nubbins relief
    translate([0, 0, -hotend_heatsink_h - hotend_heaterblock_h]) linear_extrude(hotend_heaterblock_h) hotend_heaterblock_wiring_relief();
    // print fan duct
    print_fan_duct();
    // print fan inserts
    translate(print_fan_pos) rotate([90, 0, 90]) at_fan_screws() cylinder(d=screw_insert_dia, h=screw_insert_h*2, center=true);
    // bottom plate screw holes
    translate([0, 0, bottom_z_pos]) at_bottom_pcb_screws() cylinder(d=bottom_pcb_screw_insert_dia, h=bottom_pcb_screw_insert_h);
    // bottom plate connector cutout
    translate(bottom_pcb_header_pos) cube(bottom_pcb_header_size);
    at_orbiter_plate_mounting_holes() {
        cylinder(d=screw_insert_dia, h=screw_insert_h*2, center=true);
        cylinder(d=screw_clearance_dia, h=screw_insert_h*4, center=true);
    }
    // orbiter motor screw standoff holes
    translate(orbiter_fan_pos) rotate([90, 0, 90]) {
        hull() at_orbiter_screws() cylinder(d=orbiter_motor_screw_standoff_dia, h=(orbiter_fan_pos[0] - orbiter_body_motor_side_l - base_baffle_wall_w)*2, center=true);
        at_orbiter_screws() cylinder(d=screw_clearance_dia, h=999, center=true);
    }
    // front pcb inserts
    at_front_pcb() at_front_pcb_holes() cylinder(d=screw_insert_dia, h=screw_insert_h*2, center=true);
    // carriage t slot nuts
    body_cut_t_slot_nut(delta=0);
}

module body_orbiter_shroud_cut(delta=0) {
    z = orbiter_plate_h - base_baffle_wall_w - delta;
    module peg(x, h) translate([0, orbiter_motor_y + x*(fan_side/2 - base_wall_w/2 - base_baffle_wall_w), z]) cylinder(d=base_wall_w - delta*2, h=base_baffle_wall_w + base_assembly_wiggle);
    difference() {
        union() {
            translate([orbiter_plate_size[0]/2 - delta, orbiter_motor_y - fan_side/2 - delta, z]) cube(999);
            translate([-999/2, -999/2, z + base_assembly_wiggle + base_baffle_wall_w]) cube(999);
        }
        for(x=[-1, 1]) translate([(hotend_wiring_duct_pos[0] + hotend_wiring_duct_dia/2 + orbiter_body_motor_side_l)/2, 0]) peg(x);
        translate([orbiter_body_motor_side_l + base_wall_w/2 + base_baffle_wall_w, 0]) peg(-1);
        translate([orbiter_fan_pos[0] - base_wall_w/2 - base_baffle_wall_w, 0]) peg(1);
    }
}

module body_main() difference() {
    render() body();
    body_orbiter_shroud_cut(delta=base_assembly_wiggle);
}

module body_orbiter_shroud() intersection() {
    body();
    body_orbiter_shroud_cut(delta=0);
}
