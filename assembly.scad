include <ref/derived.scad>;

$fn=20;

use <carriage.scad>;

use <orbiter.scad>;
color([0.7, 0.7, 0.7]) render() orbiter_plate();


use <fan.scad>;
translate(orbiter_fan_pos) rotate([0, 90, 0]) color([0.3, 0.3, 0.3]) render() fan();

translate(print_fan_pos) rotate([0, 90, 0]) color([0.3, 0.3, 0.3]) render() fan();

use <body.scad>;
//!render() difference() {
    union() {
        color([0.7, 0.7, 0.9]) render() body_main();
        color([0.8, 0.7, 0.9]) render() body_orbiter_shroud();
    }
    //translate([20, -999/2, -999/2]) cube(999);
//}

//translate([0, 0, orbiter_plate_h]) color([0.5, 0.5, 0.8, 0.5]) render() orbiter();

use <hotend.scad>;
color([0.6, 0.5, 0.3, 0.5]) render() hotend();
color([0.9, 0.7, 0.8]) render() translate(carriage_pos) carriage();
color([0.8, 0.7, 0.9]) render() translate(carriage_pos) carriage_belt_clamp();
color([0.6, 0.6, 0.6]) translate(carriage_pos) render() carriage_bearings(detail=true);

use <pcb.scad>;
color([1, 1, 1, 0.5]) render() translate([0, 0, bottom_z_pos]) mirror([0, 0, 1]) linear_extrude(bottom_pcb_h) bottom_pcb();
