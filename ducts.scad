include <util/round.scad>;
include <ref/derived.scad>;
use <hotend.scad>;
use <body_cuts.scad>;
use <fan.scad>;

module hotend_fan_duct(delta=0) {
    // NOTE: delta is only applied to parts that could intersect print fan duct area.
    translate(orbiter_fan_pos) rotate([-90, 0, 90]) {
        cylinder(d=orbiter_motor_dia + base_assembly_wiggle*2, h=orbiter_fan_pos[0]);
        cylinder(d=fan_dia, h=orbiter_fan_pos[0] - orbiter_body_motor_side_l - base_baffle_wall_w);
        linear_extrude(base_baffle_wall_w) round_inner(base_baffle_wall_w) difference() {
            union() {
                    fan_cut();
                    hull() mirror([1, 0]) at_orbiter_screws() circle(d=orbiter_motor_screw_standoff_dia);
            }
            at_fan_screws(4) circle(d=screw_insert_dia + base_baffle_wall_w*2);
        }
    }
    module basic(tabs) {
        difference() {
            translate(orbiter_fan_pos) rotate([-90, 0, 90]) {
                translate([0, 0, base_baffle_wall_w]) translate([0, 0, -delta]) linear_extrude(
                    orbiter_fan_pos[0] - orbiter_body_motor_side_l - base_baffle_wall_w*2 + delta*2
                ) offset(delta=delta) round_inner(hotend_heatsink_dia/4) {
                    circle(d=max(fan_dia, orbiter_motor_dia + orbiter_motor_fan_clearance*2));
                    if(tabs) translate([-hotend_heatsink_dia/4, 0]) square([left_x_pos, orbiter_motor_z + orbiter_plate_h + hotend_heatsink_h]);
                }
            }
            translate(orbiter_fan_pos) rotate([0, 90, 0]) at_fan_screws(3) hull() {
                cylinder(d=screw_insert_dia + base_baffle_wall_w*2, h=screw_insert_h*2, center=true);
                cylinder(d=screw_insert_dia, h=screw_insert_h*2 + base_baffle_wall_w*2, center=true);
            }
        }
        difference() {
            translate([0, 0, -hotend_heatsink_h - delta]) linear_extrude(
                hotend_heatsink_h + orbiter_plate_h - base_baffle_wall_w + delta*2,
            ) offset(delta=delta) round_inner(hotend_heatsink_dia/4) {
                circle(d=hotend_heatsink_dia);
                if(tabs) translate([0, orbiter_fan_pos[1] - hotend_heatsink_dia/4]) square([orbiter_fan_pos[0], hotend_heatsink_dia/2]);
                if(tabs) mirror([1, 0]) translate([0, -hotend_heatsink_dia/4]) square([999, hotend_heatsink_dia/2]);
            }
            translate([-999/2, -999/2, -hotend_heatsink_h - hotend_heaterblock_h]) cube([999, 999, hotend_heaterblock_h+base_baffle_wall_w]);
        }
    }
    difference() {
        intersection() {
            basic(tabs=true);
            rotate([-90, 0, 0]) linear_extrude(999, center=true) offset(delta=delta) round_inner(hotend_heatsink_dia/4) {
                difference() {
                    union() {
                        intersection() {
                            mirror([0, 1]) square(999);
                            projection() rotate([90, 0, 0]) basic(tabs=false);
                        }
                        round_outer(hotend_heatsink_dia/4) difference() {
                            projection() rotate([90, 0, 0]) basic(tabs=true);
                            translate([print_fan_pos[0] - base_baffle_wall_w, -base_baffle_wall_w]) {
                                square(999);
                                rotate(45) square(999);
                            }
                        }
                    }
                    mirror([1,1]) square(999);
                }
            }
        }
        transverse_hotend_wiring_duct(delta=base_baffle_wall_w);
    }
}

module orbiter_motor_wire_duct(size) translate(orbiter_fan_pos) intersection() {
    translate([-size[1]/2-base_baffle_wall_w, 0, 0]) rotate([135, 0, 0]) rotate(90) linear_extrude(999) round_outer(min(size)/3) square(size, center=true);
    cube([999, fan_side, orbiter_motor_z*2 + base_baffle_wall_w*2], center=true);
}

module orbiter_motor_installation_cutout() translate([orbiter_body_motor_side_l, orbiter_fan_pos[1], orbiter_fan_pos[2]]) {
    rotate([0, 90, 0]) linear_extrude(base_baffle_wall_w) round_inner(orbiter_motor_fan_clearance) {
        rotate(-45) translate([orbiter_motor_dia/2, 0, 0]) circle(r=orbiter_motor_fan_clearance);
        circle(d=orbiter_motor_dia);
    }
}

module orbiter_motor_wire_passthrough() {
    hull() for(z=[0,1]) translate([
        orbiter_fan_pos[0] - base_baffle_wall_w*2 - orbiter_motor_wire_dia/2,
        orbiter_fan_pos[1],
        orbiter_plate_h - base_baffle_wall_w + z*(orbiter_motor_wire_w + orbiter_motor_wire_dia/2),
    ]) intersection() {
        rotate([90, 0, 0]) cylinder(d=orbiter_motor_wire_dia, h=fan_side/2);
        cylinder(d=999, h=999);
    }
}

module transverse_hotend_wiring_duct(delta) intersection() {
    at_hotend_wiring_duct() translate([hotend_wiring_duct_dia/2 - hotend_wiring_side_access_duct_dia/2, 0, orbiter_plate_h - hotend_wiring_side_access_duct_dia/2]) minkowski(){
        mirror([1, 0, 0]) cube(999);
        sphere(d=hotend_wiring_side_access_duct_dia + delta*2);
    }
    translate([0, -999/2]) cube([999, 999, orbiter_plate_h - base_baffle_wall_w]);
}

module hotend_wiring_duct() {
    translate([0, 0, bottom_z_pos]) at_hotend_wiring_duct() cylinder(
        d=hotend_wiring_duct_dia,
        h=-bottom_z_pos + orbiter_plate_h - base_baffle_wall_w,
    );
    transverse_hotend_wiring_duct(delta=0);
}

module print_fan_duct_xy(brace) {
    r = base_wall_w;
    module s(extra) translate([left_x_pos, back_y_pos]) square([x_size + extra, -back_y_pos + print_fan_pos[1] + fan_side/2]);
    module corner_cut() polygon([
        [999, back_y_pos],
        [999, print_fan_pos[1] - fan_side/2 + base_baffle_wall_w],
        [print_fan_pos[0] - screw_insert_h - base_baffle_wall_w, print_fan_pos[1] - fan_side/2 + base_baffle_wall_w],
        [hotend_heatsink_dia/2 + max(hotend_thermistor_clearance, hotend_heater_clearance), back_y_pos],
    ]);
    module basic() difference() {
        offset(delta=-base_baffle_wall_w) s(999);
        offset(delta=base_baffle_wall_w) hotend_heaterblock_wiring_relief();
        corner_cut();
    }
    module m() difference() {
        union() {
            round_inner(r) basic();
            difference() {
                basic();
                offset(delta=r)corner_cut();
            }
        }
        children();
        translate(print_fan_pos) square([999, base_baffle_wall_w], center=true);
        if(brace) translate([left_x_pos + carriage_bearing_h - base_baffle_wall_w, 0]) square([base_baffle_wall_w, 999], center=true);
        translate(fan_pos_xy) for(y=[-1,1]) translate([0, y*fan_holes_pitch/2]) round_outer(base_baffle_wall_w) square([(screw_insert_h + base_baffle_wall_w)*2, screw_insert_dia + base_baffle_wall_w*2], center=true);
    }
    intersection() {
        round_outer(r) m() children();
        offset(delta=-base_baffle_wall_w) s(0);
    }
    intersection() {
        round_outer(r/2) m() children();
        translate([0, -999/2]) mirror([1, 0]) square(999);
    }
}

module print_fan_duct_xz() {
    h1 = hotend_heaterblock_h;
    h2 = carriage_pos[2] - bottom_z_pos;
    h3 = fan_side;
    r = hotend_heaterblock_h/2;
    round_all(r) translate([left_x_pos, bottom_z_pos]) {
        polygon([
            [base_baffle_wall_w, -999],
            [base_baffle_wall_w, h1],
            [-left_x_pos, h2],
            [x_size - (h3-h2), h2],
            [x_size - r/2, h3],
            [999, h3],
            [999, -999],
        ]);
    }
}

module print_fan_duct_hole() {
    translate(print_fan_pos) rotate([90, 0, 90]) mirror([0, 0, 1]) {
        linear_extrude(fan_pos_xy[0] - hotend_heaterblock_size[0]/2 - max(hotend_heater_clearance, hotend_thermistor_clearance) - base_baffle_wall_w - base_wall_w) difference() {
            fan_cut();
            square([base_baffle_wall_w, 999], center=true);
        }
    }
}

module print_fan_duct() {
    difference() {
        intersection() {
            union() {
                translate([0, 0, bottom_z_pos]) {
                    linear_extrude(999) print_fan_duct_xy(brace=true) bottom_pcb_insert_nubs();
                    translate([0, 0, base_wall_w]) linear_extrude(999) print_fan_duct_xy(brace=false);
                }
                print_fan_duct_hole();
            }
            rotate([90, 0, 0]) linear_extrude(999, center=true) print_fan_duct_xz();
        }
        body_cut_carriage_end_clearance(delta=base_baffle_wall_w);
        body_cut_t_slot_nut(delta=base_baffle_wall_w);
        hotend_fan_duct(delta=base_baffle_wall_w);
        translate([0, 0, bottom_z_pos]) linear_extrude(bottom_pcb_screw_insert_h + base_baffle_wall_w) bottom_pcb_insert_nubs();
        translate(bottom_pcb_header_pos) linear_extrude(bottom_pcb_header_size[2]) offset(r=base_baffle_wall_w)square([bottom_pcb_header_size[0], bottom_pcb_header_size[1]]);
    }
}

module bottom_pcb_insert_nubs() {
    at_bottom_pcb_screws() offset(r=bottom_pcb_screw_insert_dia/2 + base_baffle_wall_w) square(999);
}
