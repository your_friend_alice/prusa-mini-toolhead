include <ref/derived.scad>;

module carriage_bearing(delta=0, detail=false) {
    if(delta != 0) {
        translate([0, 0, -delta]) cylinder(d=carriage_bearing_dia + delta*2, h=carriage_bearing_h + delta*2);
    } else {
        difference() {
            cylinder(d=carriage_bearing_dia - carriage_bearing_groove_depth*2, h=carriage_bearing_h);
            if(detail) cylinder(d=8, h=999, center=true);
        }
        difference() {
            cylinder(d=carriage_bearing_dia, h=carriage_bearing_h);
            for(z=[-1, 1])translate([0, 0, carriage_bearing_h/2 + z*carriage_bearing_groove_spacing/2]) cylinder(d=999, h=carriage_bearing_groove_w, center=true);
            if(detail) cylinder(d=8, h=999, center=true);
        }
    }
}

module at_carriage_bearings() for(z=[-1,1]) translate([0, 0, z*carriage_rail_spacing/2]) rotate([0, 90, 0]) children();
module carriage_bearings(delta=0, detail=false) {
    at_carriage_bearings() carriage_bearing(delta=delta, detail=detail);
}

module carriage_belt_path(slope) {
    offset(r=carriage_belt_r) intersection_for(a=[0,1]) rotate(-a*(90-slope)) mirror([0, 1]) square(999);
}

module at_carriage_belt_anvil_halves() {
    for(x=[0,1]) mirror([x, 0]) translate([carriage_belt_min_r + carriage_belt_h - carriage_belt_tooth_h + base_wall_w/2, 0]) children();
}

module at_carriage_screws() {
    translate([carriage_bearing_h/2, 0]) for(x=[0,1]) mirror([x, 0]) translate([carriage_bearing_h/2 - t_slot_nut_l/2, 0, 0]) children();
}

module carriage_belt_anvil() {
    module strip(n) {
        for(x=[0:n-1]) translate([x*carriage_belt_pitch, carriage_belt_min_r]) belt_tooth();
    }
    // very generous upper bound on number of teeth
    n = carriage_bearing_h / carriage_belt_pitch;
    translate([0, -carriage_belt_min_r]) intersection() {
        at_carriage_belt_anvil_halves() difference() {
            carriage_belt_path(carriage_belt_slope);
            for(a=[0:carriage_belt_pitch_angle:carriage_belt_slope]) rotate(a) translate([0, carriage_belt_min_r]) belt_tooth();
            strip(n);
            rotate(carriage_belt_slope) mirror([1, 0]) strip(n);
        }
        translate([-carriage_bearing_h/2, -carriage_belt_spacing/2 + carriage_belt_z_clearance/2]) square([carriage_bearing_h, 999]);
    }
}

module carriage() {
    intersection() {
        difference() {
            hull() at_carriage_bearings() linear_extrude(carriage_bearing_h) square(carriage_bearing_dia + base_wall_w*2, center=true);
            difference() {
                union() {
                    translate([-999/2, -carriage_belt_w/2, -carriage_belt_spacing/2 + carriage_belt_z_clearance/2]) cube([999, 999, carriage_belt_spacing - carriage_belt_z_clearance/2 + carriage_belt_h - carriage_belt_tooth_h + base_baffle_wall_w]);
                    translate([-999/2, 0, -999/2]) cube(999);
                }
                translate([carriage_bearing_h/2, 0]) rotate([90, 0, 0]) {
                    linear_extrude(carriage_belt_w, center=true) {
                        translate([0, carriage_belt_spacing/2]) carriage_belt_anvil($fn=20);
                    }
                }
            }
            carriage_bearings();
            translate([0, 0, -carriage_belt_spacing/2]) rotate([90, 0, 90]) linear_extrude(999) round_outer(min(carriage_belt_y_clearance, carriage_belt_z_clearance)/3) translate([999/2-carriage_belt_y_clearance/2, 0]) square([999, carriage_belt_z_clearance], center=true);
            translate([0, 999/2, 0]) carriage_holes();
        }
        translate([0, -999/2, -999/2]) cube([carriage_bearing_h, 999, 999]);
    }
}

module carriage_holes() at_carriage_screws() rotate([90, 0, 0]) cylinder(d=screw_clearance_dia, h=999);

module carriage_belt_clamp() {
    module s() translate([-999/2, -carriage_belt_spacing/2 + carriage_belt_z_clearance/2]) square([999, carriage_belt_spacing/2 - carriage_belt_z_clearance/2 + carriage_rail_spacing/2 - carriage_bearing_dia/2]);
    module bounding() square([carriage_bearing_h, 999], center=true);
    translate([0, base_assembly_wiggle, 0]) difference() {
        translate([carriage_bearing_h/2, -carriage_belt_w/2, 0]) rotate([90, 0, 0]) mirror([0, 0, 1]) {
            linear_extrude(carriage_belt_w) intersection() {
                bounding();
                round_outer(base_wall_w/3) difference() {
                    s();
                    offset(delta=carriage_belt_h - carriage_belt_tooth_h) at_carriage_belt_anvil_halves() carriage_belt_path(90);
                }
            }
            translate([0, 0, carriage_belt_w]) linear_extrude(base_baffle_wall_w) intersection() {
                s();
                bounding();
            }
        }
        translate([0, 0, carriage_belt_spacing/2 + carriage_belt_h - carriage_belt_tooth_h + base_baffle_wall_w]) {
            mirror([0, 1, 0]) cube(999);
            translate([0, -carriage_belt_w/2 + base_baffle_wall_w/2, 0]) rotate([45, 0, 0]) mirror([0, 1, 0]) cube(999);
        }
        translate([0, 999/2, 0]) carriage_holes();
    }
}
