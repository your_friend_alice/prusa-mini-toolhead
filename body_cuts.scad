include <ref/derived.scad>;
include <util/round.scad>;
use <carriage.scad>;
use <hotend.scad>;
use <fan.scad>;
use <screw.scad>;

module body_cut_carriage_end_clearance(delta=0) {
    // main body of end part
    translate([-delta, delta, -delta]) translate([left_x_pos + carriage_bearing_h, carriage_pos[1], carriage_pos[2] - carriage_end_h/2]) difference() {
        cube([999, carriage_end_clearance, -bottom_z_pos + orbiter_plate_h]);
        translate([0, carriage_end_clearance - carriage_end_chamfer, 0]) rotate([-45, 0, 0]) translate([0, -delta/2, -999/2]) cube(999);
    }
    // the little screw nubbin
    hull() for(x=[0,1]) translate([left_x_pos + carriage_bearing_h + carriage_end_nubbin_x_pos + x*999, carriage_pos[1] + carriage_end_clearance, carriage_pos[2]]) rotate([-90, 0, 0]) cylinder(d1=carriage_end_nubbin_d1 + delta*2, d2=carriage_end_nubbin_d2 + delta*2, h=carriage_end_nubbin_h + delta);
    // blending the little screw nubbin into the t slot hole
    intersection() {
        body_cut_t_slot_nut(0);
        translate([0, carriage_pos[1] + carriage_end_clearance + carriage_end_nubbin_h, -999/2]) mirror([0, 1, 0]) cube(999);
    }
    // bearings
    translate(carriage_pos) if(delta==0) {
        // actual bearings cutout
        carriage_bearings(delta=delta);
        // clearance for the belt clamp
        translate([0, -carriage_bearing_dia/2 + carriage_belt_w/2 + base_baffle_wall_w + base_assembly_wiggle]) hull() carriage_bearings(delta=delta);
    } else {
        hull() carriage_bearings(delta=delta);
    }
}

module body_cut_t_slot_nut(delta=0) {
    translate([
        carriage_pos[0],
        carriage_pos[1] + t_slot_nut_h + carriage_belt_w/2 + base_baffle_wall_w + base_assembly_wiggle + base_wall_w,
        carriage_pos[2],
    ]) {
        carriage_holes();
        translate([carriage_bearing_h/2, 0, 0])difference() {
            rotate([90, -90, 90]) linear_extrude(
                carriage_bearing_h + t_slot_nut_l*2 + (delta==0 ? 0 : 999), center=true,
            ) offset(delta=delta + base_assembly_wiggle) t_slot_nut_outline();
            if(delta==0) cube([carriage_bearing_h - t_slot_nut_l*2, 999, 999], center=true);
        }
    }
}
