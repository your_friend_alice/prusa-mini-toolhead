include <ref/derived.scad>;
use <util/round.scad>;

module hotend(detail=true) {
    mirror([0, 0, 1]) cylinder(d=hotend_heatsink_dia, h=hotend_heatsink_h);
    heatbreak_h = detail ? 3 : 0; // not functional, just there for visual effect
    translate(hotend_heaterblock_pos) cube([hotend_heaterblock_size[0], hotend_heaterblock_size[1], hotend_heaterblock_size[2] - heatbreak_h]);
    translate([0, 0, hotend_heaterblock_pos[2]]) cylinder(d=hotend_heatsink_dia*2/3, hotend_heaterblock_h + hotend_heatsink_h);
}

module hotend_heaterblock_wiring_relief() {
    r = base_baffle_wall_w;
    round_outer(r) hull() {
        translate(hotend_heaterblock_pos) translate([hotend_heaterblock_size[0] + r/2, 0]) mirror([1, 0]) square([999, hotend_heaterblock_size[1]]);
        at_hotend_wiring_duct() circle(d=hotend_wiring_duct_dia);
        translate(hotend_heaterblock_pos) translate([hotend_heaterblock_size[0], hotend_heaterblock_back_extra_l + hotend_heatsink_dia/2]) {
            translate([0, hotend_thermistor_y_pos]) square([hotend_thermistor_clearance*2, hotend_thermistor_dia], center=true);
            translate([0, hotend_heater_y_pos]) square([hotend_heater_clearance*2, hotend_heater_dia], center=true);
        }
    }
}
