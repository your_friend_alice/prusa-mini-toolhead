$fn=90;
use <carriage.scad>;
use <body.scad>;
use <pcb.scad>;
//part="bottom_pcb";
if(part=="bottom_pcb") bottom_pcb();                    // 2d
if(part=="bottom_pcb_keepout") bottom_pcb_keepout();    // 2d
if(part=="front_pcb") front_pcb();                      // 2d
if(part=="carriage") carriage();                        // 3d
if(part=="carriage_belt_clamp") carriage_belt_clamp();  // 3d
if(part=="body") body_main();                           // 3d
if(part=="orbiter_shroud") body_orbiter_shroud();       // 3d
