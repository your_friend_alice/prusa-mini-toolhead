module round_outer(r) offset(r=r) offset(delta=-r) children();
module round_inner(r) offset(r=-r) offset(delta=r) children();
module round_all(r) round_outer(r) round_inner(r) children();

module offset_3d(r) {
    if(r==0) children();
    else if(r>0) minkowski() {
        sphere(r=r);
        children();
    } else difference() {
        cube(9999, center=true);
        minkowski() {
            sphere(r=-r);
            difference() {
                cube(99999, center=true);
                children();
            }
        }
    }
}

module round_outer_3d(r) offset_3d(r) offset_3d(-r) children();
module round_inner_3d(r) offset_3d(r=-r) offset_3d(r) children();
module round_all_3d(r) offset_3d(r) offset_3d(-r*2) offset_3d(r) children();
