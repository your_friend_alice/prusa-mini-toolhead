include <util/round.scad>;
include <ref/derived.scad>;

fan_side = 40;
fan_dia = 40;
fan_holes_pitch = 32;

module fan() {
    linear_extrude(fan_h) difference() {
        round_outer(fan_r) square(fan_side, center=true);
        at_fan_screws() circle(d=4);
        fan_cut();
    }
}

module fan_cut() {
    difference() {
        intersection() {
            offset(delta=-base_baffle_wall_w) square(fan_side, center=true);
            circle(d=fan_dia);
        }
        at_fan_screws() circle(d=screw_insert_dia + base_baffle_wall_w);
    }
}
