include <ref/derived.scad>;
module t_slot_nut_outline() translate([-t_slot_nut_flange_w/2, 0]) {
    hull() {
        translate([0, t_slot_nut_flange_h/2]) square([t_slot_nut_flange_w, t_slot_nut_flange_h/2]);
        translate([t_slot_nut_flange_h/2, 0])square([t_slot_nut_flange_w - t_slot_nut_flange_h, t_slot_nut_flange_h]);
    }
    translate([t_slot_nut_flange_w/2 - t_slot_nut_w/2, t_slot_nut_flange_h]) square([t_slot_nut_w, t_slot_nut_h - t_slot_nut_flange_h]);
}
