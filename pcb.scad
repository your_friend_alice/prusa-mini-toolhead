include <ref/derived.scad>;
use <util/general.scad>;
use <ducts.scad>;
use <hotend.scad>;
module front_pcb();

module bottom_pcb_heaterblock_projection() translate(to2d(hotend_heaterblock_pos)) square(to2d(hotend_heaterblock_size));

module bottom_pcb() {
    difference() {
        union() {
            translate(to2d(bottom_pcb_header_pos)) square(to2d(bottom_pcb_header_size));
            translate([left_x_pos, back_y_pos]) square([x_size, -back_y_pos + fan_pos_xy[1] + fan_side/2]);
        }
        at_bottom_pcb_screws() circle(d=bottom_pcb_screw_dia);
        translate(to2d(bottom_pcb_header_pos)) for(x=[0:3]) translate([bottom_pcb_header_size[0]/2 + (x-1.5)*2, bottom_pcb_header_size[1] - bottom_pcb_header_contact_point]) square([1.5, 3], center=true);
        bottom_pcb_heaterblock_projection();
    }
}

module bottom_pcb_keepout() {
    print_fan_duct_xy(brace=true) bottom_pcb_insert_nubs();
    difference() {
        hotend_heaterblock_wiring_relief();
        bottom_pcb_heaterblock_projection();
    }
}
