include <ref/derived.scad>;
include <util/round.scad>;

module orbiter_plate() difference() {
    linear_extrude(orbiter_plate_h) difference() {
        translate([0, orbiter_plate_offset]) round_outer(orbiter_plate_r) square(orbiter_plate_size, center=true);
        circle(d=orbiter_tube_dia);
        for(y=[0,1]) translate([0, y*orbiter_hole_spacing - orbiter_tube_pos]) circle(d=screw_clearance_dia);
        at_orbiter_plate_mounting_holes() circle(d=screw_clearance_dia);
        at_hotend_screws() circle(d=hotend_screw_clearance_dia);
    }
    translate([0, 0, orbiter_plate_h]) mirror([0, 0, 1]) {
        at_orbiter_plate_mounting_holes() cylinder(d1=6.25, d2=0, h=6.25/2);
        at_hotend_screws() cylinder(d=6.25, h=3);
    }
}

module orbiter() {
    translate([0, orbiter_motor_y, orbiter_motor_z]) rotate([0, 90, 0]) cylinder(d=orbiter_motor_dia, h=orbiter_motor_l);
    translate([0, -orbiter_plate_size[1]/2 + orbiter_plate_offset, 0]) {
        cube([orbiter_body_motor_side_l, orbiter_plate_size[1], orbiter_body_h]);
        mirror([1, 0, 0]) cube([orbiter_body_other_side_l, orbiter_plate_size[1], orbiter_body_h]);
    }
}
