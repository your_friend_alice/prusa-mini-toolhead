orbiter_hole_spacing = 37;
orbiter_tube_pos = 17.5;
orbiter_edge_clearance = 13;
orbiter_tube_dia = 4;
orbiter_h = 40;

orbiter_motor_dia = 36.4;
orbiter_motor_l = 33.35;
orbiter_motor_z = 19.32;
orbiter_motor_y = 5.31;
orbiter_motor_fan_clearance = 3;
orbiter_motor_connector_size = [13, 6];
orbiter_motor_wire_dia = 2;
orbiter_motor_wire_w = 4;
orbiter_body_motor_side_l = 18.75;
orbiter_body_other_side_l = 14.25;
orbiter_body_h = 39.3;

// The orbiter fan shroud attaches to the threads coming out of the orbiter motor using M3 round standoffs, with a socket head screw loctited into it.
orbiter_motor_screw_standoff_dia = 5;

orbiter_motor_screw_angle = 30;
orbiter_motor_screw_pos_r = 21.9;

orbiter_plate_size = [22.6, 47];
orbiter_plate_offset = 1;
orbiter_plate_h = 10;
orbiter_plate_r = 5;
orbiter_plate_mounting_holes = [12.6, 37];

module at_orbiter_plate_mounting_holes() for(x=[-1,1]) for(y=[-1,1]) translate([x*orbiter_plate_mounting_holes[0]/2, y*orbiter_plate_mounting_holes[1]/2 + orbiter_plate_offset]) children();

module at_orbiter_screws() for(a=[0,1]) rotate(-orbiter_motor_screw_angle + a*180) translate([orbiter_motor_screw_pos_r, 0]) children();
