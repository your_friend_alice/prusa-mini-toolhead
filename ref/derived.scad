include <screw.scad>;
include <hotend.scad>;
include <pinda.scad>;
include <base.scad>;
include <fan.scad>;
include <carriage.scad>;
include <orbiter.scad>;
include <pcb.scad>;

fan_r = (fan_side - fan_holes_pitch)/2;

orbiter_pos = [0, 0, orbiter_plate_h];

fan_pos_xy = [
    orbiter_motor_l + orbiter_motor_fan_clearance,
    orbiter_motor_y,
];
orbiter_fan_pos = [
    fan_pos_xy[0],
    fan_pos_xy[1],
    orbiter_plate_h + orbiter_motor_z,
];

print_fan_pos = [
    fan_pos_xy[0],
    fan_pos_xy[1],
    -fan_side/2
];


left_x_pos = -max(hotend_heatsink_dia/2, orbiter_body_other_side_l);
right_x_pos = fan_pos_xy[0];
x_size = -left_x_pos + right_x_pos;
carriage_pos = [
    left_x_pos,
    -orbiter_plate_size[1]/2 + orbiter_plate_offset - base_wall_w - carriage_bearing_dia/2,
    orbiter_plate_h/2 - carriage_rail_spacing/2,
];
back_y_pos = carriage_pos[1] + carriage_bearing_dia/6;
bottom_z_pos = print_fan_pos[2] - fan_side/2;
front_y_pos = orbiter_fan_pos[1] + fan_side/2;

hotend_heaterblock_size = [hotend_heaterblock_w, hotend_heatsink_dia + hotend_heaterblock_back_extra_l, hotend_heaterblock_h];
hotend_heaterblock_pos = [-hotend_heaterblock_w/2, -hotend_heatsink_dia/2 - hotend_heaterblock_back_extra_l, -hotend_heatsink_h - hotend_heaterblock_h];

orbiter_fan_tab_h = orbiter_fan_pos[2]-fan_side/2 + orbiter_motor_connector_size[0]/2*sqrt(2) + base_baffle_wall_w;

bottom_pcb_header_pos = [
    fan_pos_xy[0] - screw_insert_h - base_baffle_wall_w*2 - bottom_pcb_screw_insert_dia - bottom_pcb_header_size[0],
    front_y_pos + screw_insert_h - base_baffle_wall_w - bottom_pcb_header_size[1],
    bottom_z_pos
];

hotend_wiring_duct_pos_r = hotend_heatsink_dia/2 + hotend_wiring_duct_dia/2 + base_baffle_wall_w;
hotend_wiring_duct_pos = [hotend_wiring_duct_pos_r/sqrt(2), -hotend_wiring_duct_pos_r/sqrt(2)];
module at_hotend_wiring_duct() translate(hotend_wiring_duct_pos) children();
