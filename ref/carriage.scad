use <../util/round.scad>;
carriage_rail_spacing = 34;
carriage_belt_anchor_pos = 19.925;
carriage_belt_passthrough_pos = 11;
carriage_belt_passthrough_w = 4;
carriage_belt_passthrough_l = 13;

carriage_bearing_dia = 15;
carriage_bearing_h = 24;
carriage_bearing_groove_w = 0.8;
carriage_bearing_groove_depth = 0.25;
carriage_bearing_groove_spacing = 17.5 - 1.1;

carriage_end_clearance = 8.5;
carriage_end_nubbin_d1 = 13;
carriage_end_nubbin_d2 = 9;
carriage_end_nubbin_h = 3;
carriage_end_nubbin_x_pos = 16;
carriage_end_h = 47.8 + 1;
carriage_end_chamfer = 2;

carriage_belt_spacing = 12;
carriage_belt_z_clearance = 5;
carriage_belt_y_clearance = 8;
carriage_belt_w = 6.5;
carriage_belt_h = 1.38;
carriage_belt_pitch = 2;
carriage_belt_tooth_h = 0.75;
carriage_belt_r1 = 0.15; // radius between the tooth and the flat belt
carriage_belt_r2 = 1; // radius of the lower bulge of the tooth
carriage_belt_r2_offset = 0.4; // x offset of the bulge radius' center
carriage_belt_r3 = 0.56; // radius of the tooth tip

carriage_belt_min_r = 6;

carriage_belt_wiggle = 0.1;

carriage_belt_r = sqrt(carriage_belt_min_r^2 + (carriage_belt_pitch*2/3)^2);
carriage_belt_pitch_angle = atan(carriage_belt_pitch/2/carriage_belt_min_r)*2;
carriage_belt_slope = ceil(90/carriage_belt_pitch_angle)*carriage_belt_pitch_angle;
module belt_tooth() {
    module bottom() translate([-carriage_belt_pitch/2, 0]) square([carriage_belt_pitch, carriage_belt_h - carriage_belt_tooth_h]);
    module r3() translate([0, carriage_belt_h - carriage_belt_r3]) circle(r=carriage_belt_r3);
    module r2() for(x=[0,1]) mirror([x, 0]) intersection() {
        difference() {
            translate([-carriage_belt_r2_offset, carriage_belt_h - carriage_belt_tooth_h]) circle(r=carriage_belt_r2);
            hull() for(y=[0,1]) translate([-y*999/2, y*999]) r3();
        }
        square(999);
    }
    mirror([0, 1]) translate([0, -carriage_belt_h + carriage_belt_tooth_h]) {
        round_inner(carriage_belt_r1) {
            bottom();
            r2();
        }
        hull() {
            r2();
            r3();
        }
    }
}
