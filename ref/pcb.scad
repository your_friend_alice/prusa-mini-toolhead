pcb_w = 40;
// this is a spring loaded rectangular battery connector: Molex 0475070010
bottom_pcb_header_size = [9.5, 6, 4.5];
bottom_pcb_h = 1;
bottom_pcb_header_contact_point = 4.3;

module at_front_pcb() {
    translate([0, fan_pos_xy[1] + fan_side/2 + screw_insert_h - base_baffle_wall_w, 0])rotate([90, 0, 0]) children();
}

module at_front_pcb_holes() {
    l = screw_insert_dia/2 + base_baffle_wall_w;
    translate([fan_pos_xy[0] - l, bottom_z_pos + l]) mirror([0, 1]) children();
    translate([fan_pos_xy[0] - l, orbiter_plate_h - base_baffle_wall_w - base_assembly_wiggle - l]) children();
    translate([l-orbiter_plate_size[0]/2, -l]) mirror([1, 0]) children();
    translate([l-orbiter_plate_size[0]/2, bottom_z_pos + l]) mirror([1, 1]) children();
}
