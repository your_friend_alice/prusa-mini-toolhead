// Common screw:
// Most of the toolhead uses this screw, which is an M3 socket-head cap screw (various lengths)
screw_clearance_dia = 3.5;
screw_tap_dia = 2.8; // TODO
screw_head_dia = 6.2;
screw_head_h = 4;
screw_insert_dia = 4.7;
screw_insert_h = 5.5;

t_slot_nut_l = 10;
t_slot_nut_flange_w = 10;
t_slot_nut_w = 6;
t_slot_nut_h = 5;
t_slot_nut_flange_h = 4;

// Bottom plate screw:
// This is used to attach the bottom plate to the toolhead, it's a M2 flat-head screw.
bottom_pcb_screw_dia = 2.2;
bottom_pcb_screw_insert_dia = 3.4;
bottom_pcb_screw_insert_h = 4;

module at_bottom_pcb_screws() {
    l = bottom_pcb_screw_insert_dia/2 + base_baffle_wall_w;
    translate([left_x_pos + l, 0]) mirror([1, 0]) {
        translate([0, front_y_pos - l]) children();
        translate([0, back_y_pos + base_baffle_wall_w + l]) mirror([0, 1]) children();
    }
    translate([fan_pos_xy[0] - l, 0]) {
        translate([-screw_insert_h, front_y_pos - l]) children();
        translate([-screw_insert_h - base_baffle_wall_w - bottom_pcb_screw_dia - bottom_pcb_header_size[0] - l, front_y_pos - l]) children();
        translate([0, carriage_pos[1] + carriage_end_clearance + l]) mirror([0, 1]) children();
    }
}
