// PINDA sensor
pinda_dia = 8;
pinda_h = 35;
pinda_wire_bend_zone_l = 12;

// Steel ball:
// The PINDA sensor is held in place by a steel ball, which is pushed down against a slope and intrudes in the shaft containing the PINDA sensor.
pinda_ball_dia = 5.9;
pinda_ball_clearance_dia = 6.1;
pinda_ball_stickout_l = 1.5;
