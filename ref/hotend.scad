//tsink Dragon hotend:
// This toolhead uses the Dragon hotend, by Phaetus or Trianglelab:
// - https://www.phaetus.com/dragon-st/
// - https://www.trianglelab.net/products/dragon-hotend?VariantsId=10288

// Hotend heatsink dimensions. This is with the groove mount removed:
hotend_heatsink_dia = 22.6;
hotend_heatsink_h = 26;

// Hotend screw:
// The Dragon hotend is mounted using 10mm M2.5 socket-head cap screws, with washers.  Screw head dimensions include the washer.
hotend_screw_clearance_dia = 2.9;
hotend_screw_head_dia = 6.2;
hotend_screw_head_h = 3;
hotend_screw_pattern_r = 8;
hotend_screw_shaft_l = 10;
hotend_screw_engagement = 3;

// Heaterblock dimensions, measures from the bottom of the hotend heatsink, meaning the length of the heat break counts towards its height.
hotend_heaterblock_h = 15.5;
hotend_heaterblock_w = 21.6;
hotend_heaterblock_nozzle_h = 2.5;
// The back side of the hotend's heaterblock reaches beyond the diameter of the hotend's heatsink, so it requires extra clearance:
hotend_heaterblock_back_extra_l = 7; // TODO
hotend_heaterblock_front_inset = 2; // TODO: actual value, implementation
hotend_vent_depth = 4;
hotend_bowden_tube_d = 4;
hotend_fitting_min_flange_w = 1;
hotend_fitting_max_flange_w = 3;
hotend_fitting_cross_w = 4;
hotend_fitting_flange_h = 1.5;
hotend_fitting_chamfer = 1;
hotend_fitting_clamp_wall = 1.5;
hotend_fitting_clamp_slot = 2;

// Heaterblock nubbins y positions, measured from the center of the nozzle.
// TODO: all of these
hotend_thermistor_dia = 3;
hotend_thermistor_y_pos = 5;
hotend_thermistor_clearance = 5;
hotend_heater_dia = 8;
hotend_heater_y_pos = -12;
hotend_heater_clearance = 5;

// Hotend wiring
hotend_wiring_duct_dia = 7;
hotend_wiring_side_access_duct_dia = 5;

module at_hotend_screws() for(a=[0:3]) rotate(45 + a*90) translate([hotend_screw_pattern_r, 0]) children();
