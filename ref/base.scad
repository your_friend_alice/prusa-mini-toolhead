// Base dimensions
// walls, minimum sizes, etc

base_wall_w = 3;
base_thick_wall_w = 5;
base_baffle_wall_w = 1;
base_support_wall_w = 0.25;
base_thinnest_floor = 0.6;
base_assembly_wiggle = 0.2;
base_duct_depth = 10;
