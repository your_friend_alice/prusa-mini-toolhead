// Fan
fan_side = 40;
fan_dia = 38;
fan_holes_pitch = 32;
fan_h = 20; //TODO

module at_fan_screws(n=4) for(a=[0:n-1]) rotate(90*a) translate([fan_holes_pitch/2, fan_holes_pitch/2]) children();
